# TECHNICAL PAPER ON SQL
## SQL (Structured Query Language)

### SQL Basics

-  SQL(Structured Query Language) is a programming language used to manage and manipulate relational databases. it allows you to create, read, update, and delete the data in a database.

### Creating a Table 
```
CREATE TABLE users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE
);
```
### Inserting Data
```
INSERT INTO users (name, email)
VALUES ('John Doe', 'john@example.com'),
       ('Jane Smith', 'jane@example.com');
```

### Selecting Data
```
SELECT * FROM users;
```

### Updating Data

```
UPDATE users
SET email = 'newemail@example.com'
WHERE id = 1;

```
### Deleting Data
```
DELETE FROM users
WHERE id = 2;
```

### joins
    - joins are used to combine the rows from two tables using the column between them 
 1.  **Inner join**
    - Return the data that have matching values in both tables
 ```
 SELECT users.name, orders.order_date
FROM users
INNER JOIN orders ON users.id = orders.user_id;
 ```
2. **Left Join**
    - Returns all records from the left table and the matched records from the right table.
```
SELECT users.name, orders.order_date
FROM users
LEFT JOIN orders ON users.id = orders.user_id;
```
3. **Right Join**
    - Returns all records from the right table and the matched records from the left table.
    ```
    SELECT users.name, orders.order_date
    FROM users
    RIGHT JOIN orders ON users.id = orders.user_id;
    ```

### Aggregation

- Aggregations are used to perform calculations on multiple rows of a table and return a single value.

1. **Count**
 - Return the number of rows
 ```
SELECT COUNT(*) FROM users;
 ```
 2. **SUM**
 - Return the sum of values in a column
 ```
SELECT SUM(amount) FROM orders;
 ```
3. **AVG**
- Return the average value of the column
```
SELECT AVG(amount) FROM orders;
```
4. **MAX**
- Return the maximum value in a column
```
SELECT MAX(amount) FROM orders;
```
5. **MIN**
- Return the minimum value of the column
```
SELECT MIN(amount) FROM orders
```

### GROUP BY

- Group rows that have the same values into summary rows
```
SELECT user_id, SUM(amount) AS total_amount
FROM orders
GROUP BY user_id;
```

### References Section 

- **SQL Crash Course for Beginners** [  link](https://www.youtube.com/watch?v=DWtQU8VP3Hg)
