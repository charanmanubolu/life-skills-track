# Energy Management

## What are the activities you do that make you relax - Calm quadrant?
- Playing Board Games: Playing board games with family or friends can release the body's 'feel-good' hormones, helping to reduce stress and anxiety.
- Gardening: Spending time around plants and caring for them can help reduce negative feelings and promote relaxation.
- Movie or Show: Watching your favorite movies or TV shows can help reduce stress levels and anxiety by releasing hormones into your brain.
- Music: Listening to your favorite music can energize or quiet your mind, relax your muscles, and lift your spirit, reducing stress.

##  When do you find getting into the Stress quadrant?

- Approaching Deadlines


 - Multitasking


 - External Pressures

## How do you understand if you are in the Excitement quadrant?


- Engagement with Meaningful Work:


- Proactive Planning:

- Creative and Strategic Thinking:

 - Continuous Learning and Growth:

- Balanced Workflow:

- Positive Emotional State:


## Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.
- Memory
- Immunity
- Disease
- Emotion
- Creativity
- Errors
- Performance
- Disruption
- Repair
- Health

## What are some ideas that you can implement to sleep better? 
- Routine
- Darkness
- Temperature
- Caffeine
- Exercise

## Paraphrase the video - Brain Changing Benefits of Exercise. 

- Memory: Exercise improves the size and function of the hippocampus, enhancing memory.
- Mood: Physical activity increases the production of neurotransmitters like dopamine and serotonin, improving mood.
- Focus: Regular exercise enhances attention and concentration.
- Long-term Brain Health: Exercise reduces the risk of neurodegenerative diseases.
- Neuroplasticity: Physical activity promotes brain plasticity, leading to better cognitive function and resilience.

## What are some steps you can take to exercise more?

- Schedule
- Walk
- Stretch
- Join
- Track